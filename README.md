<p align="center"><a href="" target="_blank"><img src=https://i.postimg.cc/JnScMJJy/Logo.png" width="300"></a></p>
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200"></a></p>

---

# Les images et fichiers css / js

<h4 align="center">Les images, fichiers css / js et assets en générales sont stocker dans le dossier public</h4>
<p>Pour faire appel a ces fichiers sur Laravel, il suffit de copier ça dans un fichier <u><strong>.blade</strong></u> et mettre ton lien.</p>

* Comme ce-ci : 
```php

    {{url('images/Logo.png')}}

```

example :

```php

 <img src="{{url('images/Logo.png')}}" class="img-fluid" width="600"><img>
    

```

<h3>Les styles et js seront edité dans un premier temps dans le dossier public directement</h3>
<h4>Puis par la suite on les modifiera dans le dossier "resources" puis on passera par Webpack pour les compilier bien propres, et la sortie de Webpack se trouvera alors dans le dossier public.</h4>

---

# Les fichier Blade 

 <p>Les fichier blade sont comme des fichier HTML simple, ils sont juste parser par laravel pour les appels d'images et reconstitution de block.</p>

<p>On va séparer les block (ce que j'appelle des ) dans le dossier <strong><u>Resources/views/partials/</u></strong></p>

 * <p>Chaques partials seront a terme dans ModX des chunks</p>

 * <p>Les pages (Ressources dans ModX) sont stocker a la racine de <strong><u>"resources/views/"</u></strong></p>

 * <p>les modeles de pages sont stocker dans <strong><u>"resources/views/web"</u></strong></p>
    
    * Les modeles de pages ont besoins d'un espace pour le contenu que ont déclare comme ceci : 

        ```php
        
            @yield('content')
            
        ```

<p>Pour faire apelle a un modele de page dans une pages : 

```php

@extends('web/modeleDePage1')

@section('content')
    <h1>Contenue de la page</h1>
@endsection

```

a utiliser une fois en haut de ta page.
<br>
la partie Section et endsection sont nessecaire pour mettre le contenue dans la page.
</p>

 <p>Pour faire apelle a un partial (Chunk) de page dans une pages : 

```php

@include('partials/partial1')

```

</p>

---
# Route

Pour que les urls soit jolie il faut mettre en place des routes. Elles se trouvent dans le fichier web.php dans le dossier "routes"

ça se present de cette forme la : 

```php

Route::get('/', function () {
    return view('connexions');
});

```

juste apres le GET, le premier parametre de la fonctions get(), c'est l'url que tu mettra sur ton navigateur pour avoir acces a ta page
et dans le corps de la fonctions c'est la vue "connexions" içi qui est retourner.

<p>En gros : SI l'url est egale a "/" alors tu montre la vue "connexions"</p>

<p>Exemple :</p>

<p>Si l'url est "/groupes" alors montre la vue "groupe" : </p>

```php

Route::get('/groupes', function () {
    return view('groupe');
});

```

<p>Hesite pas a me demande si besoins</p>

<p>ps: si tu veux mettres plusieurs routes, tu les enchaine comme ceci : </p>

```php

Route::get('/', function () {
    return view('connexions');
});

Route::get('/groupes', function () {
    return view('groupe');
});

Route::get('/inscription', function () {
    return view('inscription');
});

```

---
# Pour le reste hesite pas me demandé.

## Et je vais commencé a coder regarde ce que j'ai fais, je vais mettre le plus de commentaire possible.

### Hesite pas a corriger mes fautes d'orthographe aussi Ahah
