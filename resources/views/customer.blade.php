@extends('web.model')

@section('content')

    <div class="col-12 py-4 sticky-top border-bottom bg-white">
        <div class="row">
            <div class="col-2 text-center color-red-light h3">
                <i class="far fa-long-arrow-left"></i>
            </div>
            <div class="col-8 text-center">
                <h2 class="h3 color-red-light font-quicksand bold">Carlos Ward</h2>
            </div>
            <div class="col-2 h4">
                <a href="/signalement" class="color-red-light">
                    <i class="fad fa-exclamation-triangle"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="col-12 py-2 overF-y" style="min-height: 78vh;margin-bottom: 10vh">
        <div class="message-send">
            <p>hslkfhsdm jdsmk lfhsdkf jhsdkljf</p>
        </div>
        <div class="message-receive">
            <p>jkldhfkd sjhsdli lfsdk hfsdkj hflsdkhj lsdjfl ksdlf kj</p>
        </div>
        <div class="message-send">
            <p>hslkfhsdm jdsmk lfhsdkf jhsdkljf</p>
        </div>
        <div class="message-receive">
            <p>jkldhfkd sjhsdli lfsdk hfsdkj hflsdkhj lsdjfl ksdlf kj</p>
        </div>
        <div class="message-send">
            <p>hslkfhsdm jdsmk lfhsdkf jhsdkljf</p>
        </div>
        <div class="message-receive">
            <p>jkldhfkd sjhsdli lfsdk hfsdkj hflsdkhj lsdjfl ksdlf kj</p>
        </div>
        <div class="message-send">
            <p>hslkfhsdm jdsmk lfhsdkf jhsdkljf</p>
        </div>
        <div class="message-receive">
            <p>jkldhfkd sjhsdli lfsdk hfsdkj hflsdkhj lsdjfl ksdlf kj</p>
        </div>
        <div class="message-send">
            <p>hslkfhsdm jdsmk lfhsdkf jhsdkljf</p>
        </div>
        <div class="message-receive">
            <p>jkldhfkd sjhsdli lfsdk hfsdkj hflsdkhj lsdjfl ksdlf kj</p>
        </div>
        <div class="message-send">
            <p>hslkfhsdm jdsmk lfhsdkf jhsdkljf</p>
        </div>
        <div class="message-receive">
            <p>jkldhfkd sjhsdli lfsdk hfsdkj hflsdkhj lsdjfl ksdlf kj</p>
        </div>
        <div class="message-send">
            <p>hslkfhsdm jdsmk lfhsdkf jhsdkljf</p>
        </div>
        <div class="message-receive">
            <p>jkldhfkd sjhsdli lfsdk hfsdkj hflsdkhj lsdjfl ksdlf kj</p>
        </div>
    </div>
    <div class="col-12 py-2 position-fixed mxh-20 border-top bg-white" style="left:0; bottom: 0;">
        <form action="">
            <div class="form-control">
                <div class="row">
                    <textarea class="col-9" type="text" placeholder="Ecrivez votre message içi"></textarea>
                    <button class="btn-without-style message-sender color-red-light col-3"><i
                            class="fal fa-paper-plane"></i></button>
                </div>
            </div>
        </form>
    </div>
@endsection
