@extends('web.model')

@section('content')
    <div class="col-12 color-red-light h3 py-4">
        <i class="far fa-long-arrow-left"></i>
    </div>
    <div class="col-12 mt-3 pt-3 color-red-light text-center bigger-font-size">
        <i class="fad fa-exclamation-triangle"></i>
    </div>
    <div class="col-12 text-center">
        <p class="h3 font-quicksand very-rounded border p-2 bold">Voulez vous vraiment signaler <span class="color-red-light">Carlos Ward</span> ?
        </p>
    </div>
    <div class="col-6 text-center mt-5">
        <button class="btn btn-secondary font-quicksand capitalize h1 btn-block bold shadow">NON</button>
    </div>
    <div class="col-6 text-center mt-5">
        <button class="btn btn-danger font-quicksand capitalize h1 btn-block bold shadow">OUI</button>
    </div>

@endsection
