@extends('web.model')

@section('content')

    <div class="col-12 pt-4 pb-2 sticky-top border-bottom bg-white">
        <div class="row">
            <div class="col-2 text-center color-red-light h3">
                <i class="far fa-long-arrow-left"></i>
            </div>
            <div class="col-8 text-center">
                <h3 class="h2 color-red-light"><i class="fas fa-hotel"></i></h3>
                <h2 class="h3 color-darker font-quicksand bold">Le Félix Rollet</h2>
            </div>
            <div class="col-2 h4">
                <a href="/signalement" class="color-red-light">
                    <i class="fas fa-save"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="col-12 py-4">
        <div class="row py-3">
            <label for="NameEtablissement" class="color-red-light h3 col-2">
                <i class="fas fa-feather-alt"></i>
            </label>
            <input id="NameEtablissement" type="text" class="form-control col-10" value="Le Félix Rollet">
        </div>
        <div class="row py-3">
            <label for="locationEtablissement" class="color-red-light h3 col-2">
                <i class="fas fa-map-marker-alt"></i>
            </label>
            <input id="locationEtablissement" type="text" class="form-control col-10"
                   value="1 Rue Félix Rollet, 69003 Lyon">
        </div>
        <div class="row py-3">
            <label for="mailEtablissement" class="color-red-light h3 col-2">
                <i class="fas fa-envelope"></i>
            </label>
            <input id="mailEtablissement" type="text" class="form-control col-10" value="felixrollet@mail.com">
        </div>
        <div class="row py-3">
            <label for="phoneEtablissement" class="color-red-light h3 col-2">
                <i class="fas fa-phone-alt"></i>
            </label>
            <input id="phoneEtablissement" type="text" class="form-control col-10" value="04 77 65 56 85">
        </div>
        <div class="row py-3">
            <label for="orientationEtablissement" class="color-red-light h3 col-2">
                <i class="fas fa-venus-mars"></i>
            </label>
            <input id="orientationEtablissement" type="text" class="form-control col-10" value="Hétéro - Bi">
        </div>
        <div class="row pt-4">
            <div class="color-red-light h3 col-2">
                <i class="far fa-clock"></i>
            </div>
            <div class="col-10">
                <div class="row">
                    <div class="col-6 text-center">
                        <span class="color-red-light font-quicksand">Lun.</span>
                        <span class="font-quicksand">18H - 02H</span>
                    </div>
                    <div class="col-6 text-center">
                        <span class="color-red-light font-quicksand">Jeu.</span>
                        <span class="font-quicksand">Fermé</span>
                    </div>
                    <div class="col-6 text-center">
                        <span class="color-red-light font-quicksand">Mar.</span>
                        <span class="font-quicksand">18H - 02H</span>
                    </div>
                    <div class="col-6 text-center">
                        <span class="color-red-light font-quicksand">Ven.</span>
                        <span class="font-quicksand">18H - 02H</span>
                    </div>
                    <div class="col-6 text-center">
                        <span class="color-red-light font-quicksand">Mer.</span>
                        <span class="font-quicksand">18H - 02H</span>
                    </div>
                    <div class="col-6 text-center">
                        <span class="color-red-light font-quicksand">Sam.</span>
                        <span class="font-quicksand">18H - 02H</span>
                    </div>
                    <div class="col-12 text-center">
                        <span class="color-red-light font-quicksand">Dim.</span>
                        <span class="font-quicksand">Fermé</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt-4">
            <div class="color-red-light h3 col-2">
                <i class="fas fa-calendar-day"></i>
            </div>
            <div class="col-10">
                <div class="row">
                    <div class="col-12 py-3">
                        <div class="border very-rounded">
                            <h2 class="h3 font-quicksand color-red-light text-center bold">Soirée rencontre hétéro</h2>
                            <h4 class="text-center h5 color-lighter"><small>Vendredi 29 Juin à 18H</small></h4>
                            <p class="p-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam doloremque doloribus eius fugiat iste laborum laudantium ...</p>
                        </div>
                    </div>
                    <div class="col-12 py-3">
                        <div class="border very-rounded">
                            <h2 class="h3 font-quicksand color-red-light text-center bold">Soirée rencontre hétéro</h2>
                            <h4 class="text-center h5 color-lighter"><small>Vendredi 29 Juin à 18H</small></h4>
                            <p class="p-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aperiam doloremque doloribus eius fugiat iste laborum laudantium ...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
