@extends('web.model')

@section('content')
    <div class="col-sm-12 pt-5">
        <h1 class="h3 font-quicksand bold color-darker">Inscription</h1>
    </div>
    <form class="col-sm-12 pt-4 form-group form-connexions">
        <label for="email-connexions" class="font-asap pl-2 color-red-light bold">Email</label>
        <input id="email-connexions" type="text" class="form-control color-darker" aria-describedby="emailHelp" placeholder="example@email.com">
        <br>
        <label for="password-connexions" class="font-asap pl-2 color-red-light bold">Mot de passe</label>
        <input id="password-connexions" type="password" class="form-control color-darker" aria-describedby="emailHelp">
        <br>
        <input type="checkbox" class="d-inline" aria-label="Checkbox for following text input">
        <p class="d-inline text-center">J'accepte les <a href="#" class="color-red-light bold">conditions d'utilisation</a> et <a href="#" class="color-red-light bold">Politique de confidentialité</a>.</p>
        <br>
        <btn class="btn btn-primary btn-block font-quicksand bold mt-3">Connexions</btn>
        <div class="py-5">
            <a class="color-lighter d-inline h6 pl-3" href="#">Vous avez déja un compte?</a>
            <a class="color-red-light d-inline h6 float-right pr-3" href="/">Connexion</a>
        </div>
    </form>
@endsection
