@extends('web/model')
@section('content')
    <div class="col-sm-12 text-center pt-4">
        <div class="row">
            <div class="col-2 h3 color-red-light"><i class="far fa-long-arrow-left"></i></div>
            <div class="col-8">
                <h3 class="h3 font-quicksand bold color-darker"> Paramètres </h3>
            </div>
            <div class="col-2 h3 color-red-light "><i class="fas fa-save"></i></div>
        </div>
        <br/>
        <img src="{{url('images/Profil.png')}}" class="circle mb-3" width="102px" height="102px"><img>
        <br/>
        <a class=" color-lighter" width="374" height="22" font-weight="400" font-size="14px" text-align="center"
           href="#">Appuyer pour modifier votre photo de profil</a>
    </div>

    <div id="profil" class="col-12 text-align-left font-quicksand pt-4">
        <h5>Votre Profil</h>
            <div id="parametre">
                <br/>
                <span class="col-2  color-red-light"><i class="fad fa-users color-red-light"></i></span>
                <div class="form-control">
                    <select id="ma-typo" class="select">
                        <option value="Aucun">Aucun</option>
                        <option value="Couple bi">Couple bi</option>
                        <option value="Couple gay">Couple gay</option>
                        <option value="Couple hétéro">Couple hétéro</option>
                        <option value="Couple lesbien">Couple lesbien</option>
                        <option value="Femme">Femme</option>
                        <option value="Homme">Homme</option>
                        <option value="Transgenre">Transgenre</option>
                    </select>
                </div>
                <br/>
            </div>

            <div id="parametre">
                <span class="col-2  color-red-light"><i class="fas fa-birthday-cake"></i></span>
                <div class="form-control">
                    <select id="choix-age" class="select">
                        <option value="Aucun">Aucun</option>
                        <option value="18-25">18-25</option>
                        <option value="25-45">25-45</option>
                        <option value="45-65">45-65</option>
                        <option value="65 et plus">65 et plus</option>
                    </select>
                </div>
                <br/>
            </div>

            <div id="parametre">
                <span class="col-2 color-red-light"><i class="fas fa-globe-europe"></i></span>
                <div class="form-control">
                    <select id="nationnalite" class="select">


                    </select>
                </div>
                <br/>
            </div>
 </div>

            <div class="col-12 font-quicksand pt-4">
                <h5>Description</h5>
                <div class="col-sm-12" >
                     <textarea id="description" rows="4" cols="32">Une petite description</textarea>
                </div>
            </div>

<div id="profil" class="col-12 text-align-left font-quicksand pt-4">
            <div id="parametre">
                <span class="col-2  color-red-light "><i class="fas fa-search"></i></span>
                <div class="form-control">
                    <select id="typo-recherchees" class="select">
                        <option value="Aucun">Aucun</option>
                        <option value="Couple bi">Couple bi</option>
                        <option value="Couple gay">Couple gay</option>
                        <option value="Couple hétéro">Couple hétéro</option>
                        <option value="Couple lesbien">Couple lesbien</option>
                        <option value="Femme">Femme</option>
                        <option value="Homme">Homme</option>
                        <option value="Transgenre">Transgenre</option>
                    </select>
                </div>
                <br/>
            </div>

            <div id="parametre">
                <span class="col-2  color-red-light "><i class="fas fa-eye"></i></span>
                <div class="form-control">
                    <select id="typo-recherchees" class="select">
                        <option value="Aucun">Aucun</option>
                        <option value="18-25">18-25</option>
                        <option value="25-45">25-45</option>
                        <option value="45-65">45-65</option>
                        <option value="65 et plus">65 et plus</option>
                    </select>
                </div>
                <br/>
            </div>
</div>

            <div class="col-sm-12 text-align-left pt-4">
                <br/>
                <h5>Votre abonnement</h>
                    <br/>
                    <a class="color-gold d-inline pl-2" href="#">Ka plus +</a>
                    <p class="d-inline text-center pl-2">Actif jusqu'au </p>
                    <a class="color-red-light d-inline" href="#">date</a>
                    <btn class="btn btn-secondary btn-block font-quicksand bold mt-3">Résilier</btn>
            </div>



@endsection
