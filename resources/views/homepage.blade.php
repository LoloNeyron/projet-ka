@extends('web.model')

@section('content')

    <div class="col-sm-12 position-absolute py-3 px-2" style="z-index: 25">
        <div class="navigation-laterale-toggler cursor d-inline pl-2 h4"><i class="fas fa-bars color-red-light"></i></div>
    </div>
    <div class="col-sm-12 mh-100-vh text-center" style="background-color: #ababab75">
        <h1 class="h1 font-quicksand">MAP</h1>
    </div>
    <div class="col-sm-12 position-absolute" style="z-index: 26; height: 85px;background-color: #777777;bottom:0;left:0; border-top-left-radius: 10px;border-top-right-radius: 10px">
        <div class="row mh-100">
            <div class="col-4 h2 color-red-light text-center my-auto">
                <a href="/" class="color-darker">
                    <i class="fas fa-home"></i>
                </a>
            </div>
            <div class="col-4 h2 text-center my-auto">
                <a href="/parametre" class="color-darker">
                    <i class="far fa-sliders-v"></i>
                </a>
            </div>
            <div class="col-4 h2 color-darker text-center my-auto">
                <a href="/" class="color-darker">
                    <i class="fas fa-location-arrow"></i>
                </a>
            </div>
        </div>
    </div>

@endsection
