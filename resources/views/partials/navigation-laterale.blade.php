<div id="navigation-laterale" class="animate bg-transparent mh-100-vh" style="width: 100vw;position: absolute;left:-200vw;top:0;z-index: 555">
    <div class="container-fluid p-0 mh-100-vh bg-transparent">
        <div class="row mh-100-vh">
            <div class="col-8 py-4 px-0 mh-100-vh bg-white" style="box-shadow: 29px 11px 22px rgba(2, 31, 84, 0.13);">
                <div class="col-12">
                    <div class="navigation-laterale-toggler cursor d-inline pl-2 h4 color-red-light">
                        <i class=" fas fa-times" style="z-index: 800"></i>
                    </div>
                </div>
                <div class="col-12 text-center p-0">
                    <img src="{{url('images/Profil.png')}}" alt="" class="circle mb-3" width="69" height="69">
                    <h3 class="h3 color-red-light font-quicksand bold color-darker">Johanna Doe</h3>
                    <p class="color-lighter font-asap">johanna@compagny.com</p>
                    <br>
                </div>
                <div class="col-sm-12 p-0">
                    <ul class="list-style-none m-0 pl-1">
                        <li class="py-3 active">
                            <a href="" class="col-12 pl-1">
                                <span class="col-3 h4 color-light ico active"><i class="fas fa-home-alt"></i></span>
                                <span class="col-6 bold font-asap low-font-size p-0 color-darker text active">Accueil</span>
                                <span class="col-3 h4 float-right color-light ico animate carret active "><i class="fas fa-chevron-right"></i></span>
                            </a>
                        </li>
                        <li class="py-3">
                            <a href="" class="col-12 pl-1">
                                <span class="col-3 h4 color-light ico"><i class="fad fa-chart-bar"></i></span>
                                <span class="col-6 bold font-asap low-font-size p-0 color-darker text">Vos statistiques</span>
                                <span class="col-3 h4 float-right color-light ico animate carret"><i class="fas fa-chevron-right"></i></span>
                            </a>
                        </li>
                        <li class="py-3">
                            <a href="" class="col-12 pl-1">
                                <span class="col-3 h4 color-light ico"><i class="fas fa-user-alt"></i></span>
                                <span class="col-6 bold font-asap low-font-size p-0 color-darker text">Votre profil</span>
                                <span class="col-3 h4 float-right color-light ico animate carret"><i class="fas fa-chevron-right"></i></span>
                            </a>
                        </li>
                        <li class="py-3">
                            <a href="/etablissement" class="col-12 pl-1">
                                <span class="col-3 h4 color-light ico"><i class="fas fa-hotel"></i></span>
                                <span class="col-6 bold font-asap low-font-size p-0 color-darker text">Etablissement</span>
                                <span class="col-3 h4 float-right color-light ico animate carret"><i class="fas fa-chevron-right"></i></span>
                            </a>
                        </li>
                        <li class="py-3">
                            <a href="" class="col-12 pl-1">
                                <span class="col-3 h4 color-light ico"><i class="fal fa-shopping-bag"></i></span>
                                <span class="col-6 bold font-asap low-font-size p-0 color-darker text color-gold">K plus +</span>
                                <span class="col-3 h4 float-right color-light ico animate carret"><i class="fas fa-chevron-right"></i></span>
                            </a>
                        </li>
                        <li class="py-3">
                            <a href="" class="col-12 pl-1">
                                <span class="col-3 h4 color-light ico"><i class="fas fa-user-friends"></i></span>
                                <span class="col-6 bold font-asap low-font-size p-0 color-darker text">Vos amis</span>
                                <span class="col-3 h4 float-right color-light ico animate carret"><i class="fas fa-chevron-right"></i></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 p-0 position-absolute" style="bottom:35px;left:0;">
                    <ul class="list-style-none m-0 pl-1">
                        <li class="py-3">
                            <a href="" class="col-12 pl-1">
                                <span class="col-3 h4 color-light ico"><i class="fas fa-cogs"></i></span>
                                <span class="col-9 bold font-asap low-font-size p-0 color-darker text">Paramètre</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="navigation-laterale-backdrop" class="col-4 navigation-laterale-toggler animate" style="background: #3b599835;"></div>
        </div>
    </div>
</div>
