@extends('web.model')

@section('content')

    <div class="col-sm-12 text-center pt-4">
        <img src="{{url('images/Logo.png')}}" alt="" class="img-fluid" width="182" height="110">
    </div>
    <div class="col-sm-12 py-3 mt-3 mx-2 text-center border-red bg-red-light">
        <h5 class="h5 color-red-light">Indentification ou mot de passe erroné</h5>
    </div>


    <div class="col-sm-12 pt-5">
        <h1 class="h3 font-quicksand bold color-darker">Connexions</h1>
        <p class="color-lighter font-asap">Ravi de vous revoir parmi nous.</p>
    </div>
    <form class="col-sm-12 pt-4 form-group form-connexions">
        <label for="email-connexions" class="font-asap pl-2 color-red-light bold">Email</label>
        <input id="email-connexions" type="text" class="form-control color-darker" aria-describedby="emailHelp"
               placeholder="example@email.com">
        <br>
        <label for="password-connexions" class="font-asap pl-2 color-red-light bold">Mot de passe</label>
        <input id="password-connexions" type="password" class="form-control color-darker" aria-describedby="emailHelp">
        <br>
        <btn class="btn btn-primary btn-block font-quicksand bold mt-3">Connexions</btn>
    </form>
    <div class="col-sm-12 py-5">
        <a class="color-lighter d-inline h6 pl-3" href="#">Mot de passe oublié?</a>
        <a class="color-red-light d-inline h6 float-right pr-3" href="/inscription">Inscription</a>
    </div>

@endsection
