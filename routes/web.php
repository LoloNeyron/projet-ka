<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('connexions');
});

Route::get('/home', function () {
    return view('homepage');
});

Route::get('/inscription', function () {
    return view('inscription');
});

Route::get('/customer', function () {
    return view('customer');
});

Route::get('/signalement', function () {
    return view('signalement');
});

Route::get('/etablissement', function () {
    return view('etablissement');
});

Route::get('/amis', function () {
    return view('amis');
});

Route::get('/parametre', function () {
    return view('parametre');
});

